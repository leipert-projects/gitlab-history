#!/usr/bin/env node

import fs from 'fs';
import path from 'path';
import { fileURLToPath } from 'url';
import {
  languages as languageDefinitions,
  handbookCategories as handbookCategoryDefinitions,
} from './assets/descriptions.mjs';
import _ from 'lodash';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const gitlabData = JSON.parse(
  fs.readFileSync(path.join(__dirname, 'results', 'lang.json'))
);
const handbookData = JSON.parse(
  fs.readFileSync(path.join(__dirname, 'results', 'handbook.json'))
);

function percent(pretty, total) {
  return Math.round((pretty / total) * 10000) / 100;
}

const result = { series: {}, Handbook: {} };

const languages = _(gitlabData)
  .map(release => Object.keys(release))
  .flatten()
  .uniq()
  .without('header', 'SUM')
  .value();

const handbookCategories = _(handbookData)
  .reverse()
  .map(release => Object.keys(release))
  .flatten()
  .map(v => v.replace(/_/, '-'))
  .uniq()
  .without('header', 'other')
  .value();

handbookCategories.unshift('other');

const diffLanguages = _.difference(languages, Object.keys(languageDefinitions));
const diffHandbook = _.difference(
  handbookCategories,
  Object.keys(handbookCategoryDefinitions)
);

let ERRORS = false;

if (diffLanguages.length > 0) {
  ERRORS = `Unknown languages in GitLab: ${diffLanguages}`;
}
if (diffHandbook.length > 0) {
  ERRORS = ERRORS ? `${ERRORS}\n` : '';
  ERRORS += `Unknown handbook sections: ${diffHandbook}`;
}
if (ERRORS) {
  console.warn(ERRORS);
  process.exit(1);
}

handbookCategories.forEach(category => {
  result.Handbook[category] = [];

  const averageWordCount = [];

  const sumUp = function(release, index) {
    const tag = release.header.tag;

    const words = _.get(release, [category, 'wordCount'], 0);
    const files = _.get(release, [category, 'fileCount'], 0);
    const sumWords = _.reduce(
      release,
      (agg, cat) => agg + (cat.wordCount || 0),
      0
    );
    const sumFiles = _.reduce(
      release,
      (agg, cat) => agg + (cat.fileCount || 0),
      0
    );

    const data = {
      wordCount: words,
      wordsPerFile: 0,
      fileCount: files,
      tag,
    };

    averageWordCount.push({
      wordCount: 0,
      fileCount: 0,
      wordsPerFile: Math.round(sumWords / sumFiles),
      tag,
    });

    result.Handbook[category].push(data);
  };

  result.Handbook.average = averageWordCount;

  handbookData.forEach(sumUp);
});

languages.forEach(language => {
  result.series[language] = [];

  let previous = {
    locAbsolute: 0,
    filesAbsolute: 0,
    locPercentage: 0,
    filesPercentage: 0,
  };

  let series = [];

  gitlabData.forEach((release, index) => {
    const tag = release.header.tag;

    const loc = _.get(release, [language, 'code'], 0);
    const files = _.get(release, [language, 'nFiles'], 0);

    let locPercentage = 0;

    let prevIndex = index - 3;

    if (prevIndex >= 0 && loc > 0) {
      const beforeLoc = series[prevIndex].locAbsolute;
      if (beforeLoc > 0) {
        locPercentage = percent(loc - beforeLoc, beforeLoc);
      }
    }

    const data = {
      locAbsolute: loc,
      locDiff: loc - previous.locAbsolute,
      locGrowth: locPercentage,
      locPercentage: percent(loc, _.get(release, ['SUM', 'code'])),
      filesAbsolute: files,
      filesDiff: files - previous.filesAbsolute,
      filesPercentage: percent(files, _.get(release, ['SUM', 'nFiles'])),
      locPerFile: _.round(loc / files, 2),
      tag,
    };

    result.series[language].push(data);

    previous = data;
    series.push(data);
  });
});

result.releases = gitlabData.map(release => release.header.tag);
result.dateByTag = gitlabData.reduce((agg, release) => {
  agg[release.header.tag] = release.header.date * 1000;

  return agg;
}, {});

result.tagByDate = _.invert(result.dateByTag);

result.languages = languages;

fs.writeFileSync(path.join('assets', 'results.json'), JSON.stringify(result));
