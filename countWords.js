#!/usr/bin/env node

const program = require('commander');

program
  .usage('<dir ...>')
  .option('--output <file>', 'Output file')
  .option('--date <date>', 'Date')
  .option('--tag <tag>', 'tag')
  .parse(process.argv);

const bail = e => {
  console.warn(e.message || e);
  process.exit(1);
};

if (!program.args.length) {
  bail('Please provide at least one dir');
}

const path = require('path');
const fs = require('fs-extra');
const globby = require('globby');
const htmlToText = require('html-to-text');
const Promise = require('bluebird');
const wordCount = require('wordcount');
var md = require('markdown-it')({
  html: true, // Enable HTML tags in source
}).use(require('markdown-it-front-matter'), function(fm, cb) {
  //noop
});

const ensurePathsExists = args =>
  Promise.all(
    args.map(p => {
      const absolutePath = path.resolve(p);

      return fs.pathExists(absolutePath).then(exists => {
        if (exists) {
          return absolutePath;
        }
        throw new Error(
          `Path ${p} (resolved to ${absolutePath}) does not exist`
        );
      });
    })
  );

const createGlobs = paths =>
  paths
    .map(p => path.join(p, '**/*.{html,md}'))
    .concat('!**/CHANGELOG.html.md');

const HTMLToText = html =>
  htmlToText.fromString(html, {
    wordwrap: false,
    ignoreImage: true,
    ignoreHref: true,
  });

const convertToText = file => {
  if (file.endsWith('.html')) {
    return fs.readFile(file, 'utf8').then(HTMLToText);
  } else if (file.endsWith('.md')) {
    return fs
      .readFile(file, 'utf8')
      .then(content => md.render(content))
      .then(HTMLToText);
  }
};

const groupFiles = (paths, allFiles) => {
  const perCategory = allFiles.reduce((agg, curr) => {
    let relative = curr;

    paths.forEach(p => {
      if (curr.startsWith(p)) {
        relative = path.relative(p, curr);
      }
    });

    relative = relative.split(path.sep);

    const category = relative[0];

    if (!agg[category]) {
      agg[category] = { key: category, files: [] };
    }

    agg[category].files.push(curr);

    return agg;
  }, {});

  return Object.values(perCategory);
};

const getData = categories =>
  Promise.all(
    categories.map(({ files, key }) => {
      return Promise.props({
        fileCount: files.length,
        key: key.replace(/_/g, '-'),
        wordCount: Promise.all(files.map(convertToText))
          .then(files => files.join('\n\n'))
          .then(wordCount),
      });
    })
  );

const wrapCategories = categories => {
  return categories.reduce(
    (agg, curr) => {
      agg[curr.key] = curr;
      return agg;
    },
    { header: { date: parseInt(program.date, 10), tag: program.tag } }
  );
};

const getTop = topX => list => {
  const top = list.sort((a, b) => b.wordCount - a.wordCount);

  if (top.length <= topX) {
    return top;
  }

  const other = top.slice(topX - 1).reduce(
    (agg, curr) => {
      agg.wordCount += curr.wordCount;
      agg.fileCount += curr.fileCount;
      return agg;
    },
    { key: 'other', fileCount: 0, wordCount: 0 }
  );

  return top.slice(0, topX - 1).concat([other]);
};

const parseFiles = paths => {
  return globby(createGlobs(paths))
    .then(files => groupFiles(paths, files))
    .then(getData)
    .then(getTop(12))
    .then(wrapCategories);
};

const log = data => {
  console.warn(data);
  return data;
};

const output = data => {
  if (program.output) {
    return fs
      .ensureDir(path.dirname(program.output))
      .then(fs.writeFile(program.output, JSON.stringify(data), 'utf8'));
  }
  return data;
};

ensurePathsExists(program.args)
  .then(parseFiles)
  .then(log)
  .then(output)
  .catch(bail);
