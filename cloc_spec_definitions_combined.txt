CoffeeScript_spec
    filter remove_html_comments
    extension coffee_spec
    3rd_gen_scale 1
Go_spec
    filter remove_html_comments
    extension go_spec
    3rd_gen_scale 1
JavaScript_spec
    filter remove_html_comments
    extension es6_spec
    3rd_gen_scale 1
Ruby_spec
    filter remove_html_comments
    extension ruby_spec
    3rd_gen_scale 1
SVG
    filter remove_html_comments
    extension svg
    3rd_gen_scale 1
