import moment from 'moment';

import chartjs from 'chart.js';

import { languages, handbookCategories } from './descriptions';

const Color = chartjs.helpers.color;

const getColor = key => {
  if (languages[key] && languages[key].color) {
    const colorHash = languages[key].color;
    return key.includes('_spec')
      ? new Color(colorHash).darken(0.2).rgbString()
      : colorHash;
  }

  if (handbookCategories[key] && handbookCategories[key].color) {
    return handbookCategories[key].color;
  }

  throw new Error(`Unknown category/language: ${key}`);
};

export const convertToDataset = (
  series,
  dataset,
  currentType,
  fill = false,
  dateRange = [],
  formatLabel = false
) => {
  const datasets = _.map(series, (value, languageKey) => {
    const language = languages[languageKey] || {};
    const color = getColor(languageKey);
    const [startDate, endDate] = dateRange;
    const data = value
      .map(datapoint => {
        datapoint.x = dataset.dateByTag[datapoint.tag];
        datapoint.y = datapoint[currentType];
        return datapoint;
      })
      .filter(
        datapoint =>
          !(
            (startDate && datapoint.x < startDate) ||
            (endDate && datapoint.x > endDate)
          )
      );

    return {
      borderColor: color,
      backgroundColor: new Color(color).alpha(0.7).rgbString(),
      borderWidth: fill ? 1 : 3,
      data,
      yAxisID: 'y-axis-0',
      fill: fill ? '-1' : false,
      label: language.title || languageKey,
      formatLabel: formatLabel
        ? data => {
            if (data.y === 0) {
              return '';
            }
            return formatLabel(languageKey, data);
          }
        : undefined,
    };
  }).reverse();

  if (datasets[0]) {
    datasets[0].fill = fill ? 'start' : false;
  }

  return datasets;
};

export const convertToAnnotations = (data, enabledTypes) => {
  return data
    .filter(event => {
      return enabledTypes.includes(event.type);
    })
    .map(event => {
      return {
        drawTime: 'afterDatasetsDraw',
        type: 'line',
        mode: 'vertical',
        scaleID: 'x-axis-0',
        value: moment(event.date),
        borderColor: 'black',
        borderWidth: 1,
        label: {
          position: 'top',
          content: event.icon,
          enabled: true,
        },
      };
    });
};

const getChartConfig = (dataset, currentType) => {
  const datasets = convertToDataset(dataset.series, dataset, currentType);

  const annotations = [];

  return {
    type: 'line',
    data: {
      datasets,
    },
    options: {
      annotation: {
        annotations,
      },
      pan: {
        enabled: true,
        mode: 'xy',
        speed: 20,
        threshold: 10,
      },
      zoom: {
        enabled: true,
        mode: 'x',
        limits: {
          max: 10,
          min: 0.5,
        },
      },
      responsive: true,
      title: {
        display: false,
      },
      legend: {
        display: false,
        reverse: true,
        position: 'bottom',
        onClick: () => {},
      },
      tooltips: {
        position: 'nearest',
        mode: 'index',
        intersect: false,
        displayColors: true,
        itemSort: (a, b) => {
          return a.datasetIndex > b.datasetIndex ? -1 : 1;
        },
        callbacks: {
          footer(tooltipItems, ref) {
            const tooltipItem = tooltipItems[0];
            const total = Math.round(
              tooltipItems.reduce((sum, el) => sum + el.yLabel, 0)
            );
            return `Total: ${total}`;
          },
          title(tooltipItems, ref) {
            const tooltipItem = tooltipItems[0];
            const currDS = ref.datasets[tooltipItem.datasetIndex];
            const item = currDS.data[tooltipItem.index];
            return `${item.tag} (${moment(dataset.dateByTag[item.tag]).format(
              'YYYY-MM'
            )})`;
          },
          label: function(tooltipItem, ref) {
            const dataset = ref.datasets[tooltipItem.datasetIndex];
            const item = dataset.data[tooltipItem.index];
            if (dataset.formatLabel) {
              return dataset.formatLabel(item);
            }
            return '';
          },
        },
      },
      elements: {
        line: {
          tension: 0.000001,
        },
        point: {
          radius: 0,
          hoverRadius: 0,
        },
      },
      plugins: {
        filler: {
          propagate: false,
        },
      },
      scales: {
        xAxes: [
          {
            id: 'x-axis-0',
            type: 'time',
            display: true,
            ticks: {
              autoSkip: true,
              source: 'data',
              callback: function(value, index, values) {
                const date = values[index].value;
                const tag = dataset.tagByDate[date];
                return `${tag} (${moment(date).format('YYYY-MM')})`;
              },
            },
            time: {},
          },
        ],
        yAxes: [
          {
            stacked: false,
            type: 'linear',
            display: true,
            position: 'right',
            id: 'y-axis-0',
            scaleLabel: {
              display: true,
              labelString: 'Lines of Code',
            },
            ticks: {},
            gridLines: {
              drawOnChartArea: true,
            },
          },
        ],
      },
    },
  };
};

export default getChartConfig;
