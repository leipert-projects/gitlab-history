const ALPHA = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

const mapData = (raw, type, letters) =>
  raw
    .sort((a, b) => {
      if (a.date === b.date) {
        return 0;
      }
      return a.date < b.date ? -1 : 1;
    })
    .map((event, index) => {
      event.icon = letters.length > 1 ? letters[index] : letters + (index + 1);
      event.type = type;
      return event;
    });

const generalRaw = [
  {
    date: '2012-08-24',
    link: 'https://news.ycombinator.com/item?id=4428278',
    title: '"Show HN" thread announcing gitlab.io',
  },
  {
    date: '2013-07-22',
    title: 'GitLab EE announced',
    link:
      'https://about.gitlab.com/2013/07/22/announcing-gitlab-enterprise-edition/',
  },
  {
    date: '2015-03-04',
    link:
      'https://about.gitlab.com/2015/03/04/gitlab-is-part-of-the-y-combinator-family/',
    title: 'GitLab is part of the Y Combinator family',
  },
  {
    date: '2015-09-17',
    link:
      'https://about.gitlab.com/2015/09/17/gitlab-announces-4m-series-a-funding-from-khosla-ventures/',
    title: 'Series A announced',
  },
  {
    date: '2015-10-29',
    link:
      'https://about.gitlab.com/2015/10/29/one-million-downloads-of-gitlab/',
    title: '1 Million Downloads of GitLab',
  },
  {
    date: '2016-09-13',
    link: 'https://about.gitlab.com/2016/09/13/gitlab-master-plan/',
    title: 'Series B announced',
  },
  {
    date: '2017-10-07',
    link:
      'https://about.gitlab.com/2017/10/09/gitlab-raises-20-million-to-complete-devops/',
    title: 'Series C announced',
  },
  {
    date: '2018-09-19',
    link:
      'https://about.gitlab.com/2018/09/19/announcing-100m-series-d-funding/',
    title: 'Series D announced',
  },
];

const summits = [
  {
    date: '2013-10-15',
    title: 'Summit in Novi Sad, Serbia',
  },
  {
    date: '2015-10-15',
    title: 'Summit in Amsterdam, The Netherlands',
  },
  {
    date: '2016-05-15',
    title: 'Summit in Austin, Texas',
  },
  {
    date: '2017-01-15',
    title: 'Summit in Cancun, Mexico',
  },
  {
    date: '2017-10-15',
    title: 'Summit in Crete, Greece',
  },
  {
    date: '2018-08-15',
    title: 'Summit in Cape Town, South Africa',
  },
];

const frontend = [
  {
    date: '2015-12-07',
    title: 'First Frontend Developer joins GitLab',
  },
  {
    date: '2016-06-30',
    title: 'First Vue Component',
    link:
      'https://gitlab.com/gitlab-org/gitlab-foss/commit/af5fc6e24ce3e2f5fa42d6764a780afe0d6471d9',
  },
  {
    date: '2016-07-24',
    title: 'Migration from CoffeeScript to ES6',
    link:
      'https://gitlab.com/gitlab-org/gitlab-foss/commit/aaa9509d120524573085e94af9de5cdde83e3271',
  },
  {
    date: '2016-10-18',
    title: 'Added webpack to the build chain',
    link:
      'https://gitlab.com/gitlab-org/gitlab-foss/commit/4c5ff1d08ef5ddb7db432b864e18dd7674fbc116',
  },
  {
    date: '2017-04-06',
    title: 'First single file Vue component',
    link:
      'https://gitlab.com/gitlab-org/gitlab-foss/commit/909718414b4399497d67c86aa5f7f6ca7296ea0e',
  },
  {
    date: '2017-06-08',
    title: 'Added vuex',
    link:
      'https://gitlab.com/gitlab-org/gitlab-foss/commit/f7344338985338df3d0958ef9d362b9624723e0f',
  },
  {
    date: '2017-12-20',
    title: 'Added prettier',
    link: 'https://gitlab.com/gitlab-org/gitlab-foss/merge_requests/16061',
  },
];

const backend = [
  {
    date: '2012-09-04',
    title: 'Marin joined',
    link: 'https://about.gitlab.com/2012/09/04/welcome-marin/',
  },
  {
    date: '2015-05-03',
    title: 'GitLab runner becomes official',
    link:
      'https://about.gitlab.com/2015/05/03/unofficial-runner-becomes-official/',
  },
  {
    date: '2015-07-25',
    title: 'First commit to GitLab Workhorse',
    link:
      'https://gitlab.com/gitlab-org/gitlab-workhorse/commit/95de37db75e0679c374c5c78e84cbf77fcd9e653',
  },
  {
    date: '2016-11-14',
    title: 'First commit to Gitaly',
    link:
      'https://gitlab.com/gitlab-org/gitaly/commit/b7e24f70b71ae8aeb0b6a0d91bf49960e17cb3a3',
  },
  {
    date: '2015-11-23',
    title: 'Git LFS Support announced',
    link:
      'https://about.gitlab.com/2015/11/23/announcing-git-lfs-support-in-gitlab/',
  },
];

export default [
  ...mapData(generalRaw, 'general', ALPHA),
  ...mapData(summits, 'summit', 's'),
  ...mapData(backend, 'backend', 'b'),
  ...mapData(frontend, 'frontend', 'f'),
];
