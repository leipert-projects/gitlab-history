export const handbookCategories = {
  other: { color: '#a9a9a9' },
  marketing: { color: '#800000' },
  engineering: { color: '#4363d8' },
  'business-ops': { color: '#ffd8b1' },
  product: { color: '#42d4f4' },
  support: { color: '#f58231' },
  hiring: { color: '#e6194B' },
  'people-operations': { color: '#911eb4' },
  'people-group': { color: '#911eb4' },
  finance: { color: '#469990' },
  'customer-success': { color: '#ffe119' },
  sales: { color: '#ffd8b1' },
  infrastructure: { color: '#4363d8' },
  'sales-training': { color: '#ffd8b1' },
  'general-onboarding': { color: '#911eb4' },
  communication: { color: '#a9a9a9' },
  'index.html.md': { color: '#a9a9a9' },
  'sales-process': { color: '#ffd8b1' },
  contracts: { color: '#DDD' },
  accounting: { color: '#469990' },
  'index.md': { color: '#a9a9a9' },
  'stock-options': { color: '#DDD' },
  security: { color: '#4363d8' },
  operations: { color: '#469990' },
  'positioning-faq': { color: '#3cb44b' },
  'sales-qualification-questions': { color: '#ffd8b1' },
  'developer-onboarding': { color: '#4363d8' },
  demo: { color: '#3cb44b' },
  'human-resources': { color: '#911eb4' },
  'index.html': { color: '#a9a9a9' },
  'create-directory': { color: '#3cb44b' },
  'support-and-development-process': { color: '#f58231' },
  'sales-onboarding': { color: '#ffd8b1' },
  'dev-onboarding': { color: '#4363d8' },
  average: { color: '#000' },
  'board-meetings': { color: '#911eb4' },
  offboarding: { color: '#911eb4' },
  'new-hire-day-one-us-employees-only': { color: '#911eb4' },
  ceo: { color: '#42d4f4' },
};
export const languages = {
  Ruby: { color: '#e6194B', groups: ['backend'] },
  Ruby_spec: { color: '#e6194B', title: 'Ruby (Tests)', groups: ['backend'] },
  Go: { color: '#911eb4', groups: ['backend'] },
  Go_spec: { color: '#911eb4', title: 'Go (Tests)', groups: ['backend'] },
  Haml: { color: '#469990', groups: ['backend', 'frontend'] },
  JavaScript: { color: '#ffe119', groups: ['frontend'] },
  JavaScript_spec: {
    color: '#ffe119',
    title: 'JavaScript (Tests)',
    groups: ['frontend'],
  },
  CoffeeScript: { color: '#9A6324', groups: ['frontend'] },
  CoffeeScript_spec: {
    color: '#9A6324',
    title: 'CoffeeScript (Tests)',
    groups: ['frontend'],
  },
  'Vuejs Component': { color: '#808000', title: 'Vue', groups: ['frontend'] },
  Sass: { color: '#f58231', groups: ['frontend'] },
  SVG: { color: '#800000', groups: ['frontend'] },
  GraphQL: { color: '#a9a9a9', groups: ['frontend'] },
  CSS: { color: '#ffd8b1' },
  ERB: { color: '#42d4f4' },
  HTML: { color: '#4363d8' },
  Cucumber: { color: '#3cb44b' },
  TOML: { color: '#a9a9a9' },
  'Jupyter Notebook': { color: '#800000' },
};

const getLanguageTitle = languageKey => {
  return (
    (languages[languageKey] && languages[languageKey].title) || languageKey
  );
};

const signedToString = number => {
  return number > 0 ? `+${number}` : `${number}`;
};

export const datasets = {
  locAbsolute: {
    source: 'GitLab',
    title: 'LoC count',
    description: 'Lines of Code (absolute)',
    formatLabel: (key, data) => `${getLanguageTitle(key)}: ${data.y} LoC`,
  },
  locGrowth: {
    source: 'GitLab',
    title: 'LoC (quarterly growth in %)',
    description: 'Lines of Code (quarterly growth)',
    formatLabel: (key, data) =>
      `${getLanguageTitle(key)}: ${signedToString(data.y)} %`,
  },
  locDiff: {
    source: 'GitLab',
    title: 'LoC (added/removed)',
    description: 'Lines of Code (diff to previous month)',
    formatLabel: (key, data) =>
      `${getLanguageTitle(key)}: ${signedToString(data.y)} LoC`,
  },
  locPercentage: {
    source: 'GitLab',
    title: 'LoC distribution in %',
    description: 'Lines of Code distribution (normalized to whole code base)',
    formatLabel: (key, data) => `${getLanguageTitle(key)}: ${data.y} %`,
  },
  filesAbsolute: {
    source: 'GitLab',
    title: 'Files count',
    description: 'Files (absolute)',
    formatLabel: (key, data) => `${getLanguageTitle(key)}: ${data.y} files`,
  },
  filesDiff: {
    source: 'GitLab',
    title: 'Files (added/removed)',
    description: 'Files (diff to previous month)',
    formatLabel: (key, data) =>
      `${getLanguageTitle(key)}: ${signedToString(data.y)} files`,
  },
  filesPercentage: {
    source: 'GitLab',
    title: 'Files distribution in %',
    description: 'Files distribution (normalized to whole code base)',
    formatLabel: (key, data) => `${getLanguageTitle(key)}: ${data.y} %`,
  },
  locPerFile: {
    source: 'GitLab',
    title: 'LoC per file',
    description: 'Lines of Code per file',
    formatLabel: (key, data) => `${getLanguageTitle(key)}: ${data.y} LoC/File`,
  },
  wordCount: {
    source: 'Handbook',
    title: 'Word count',
    description: 'Handbook: Total amount of words per category',
    formatLabel: (key, data) => `${getLanguageTitle(key)}: ${data.y} words`,
  },
  wordsPerFile: {
    source: 'Handbook',
    title: 'Words per file',
    description: 'Handbook: Average words per file per category',
    formatLabel: (key, data) => `${getLanguageTitle(key)}: ${data.y} words`,
  },
  fileCount: {
    source: 'Handbook',
    title: 'File count',
    description: 'Handbook: Files per category',
    formatLabel: (key, data) => `${getLanguageTitle(key)}: ${data.y} files`,
  },
};

export const events = {
  general: {
    title: 'General',
  },
  summit: {
    title: 'Summits',
  },
  backend: {
    title: 'Backend',
  },
  frontend: {
    title: 'Frontend',
  },
};
