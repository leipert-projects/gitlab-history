#!/usr/bin/env bash

#!/bin/bash
# http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -euo pipefail

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

GITLAB_DIR="$DIR/repositories/gitlab"
GITLAB_FOSS_DIR="$DIR/repositories/gitlab-foss"
CURR_DIR="$GITLAB_FOSS_DIR"

SWITCH="SWITCH"

CI_BUILD_DATE=$(date '+%s')
CI=${CI:-false}
if [ "${CI}" == "true" ]; then
  BUILD_ALL_TAGS="$CI"
  BUILD_OVERWRITE_DATE="$CI"
else
  BUILD_ALL_TAGS=${BUILD_ALL_TAGS:-false}
  BUILD_OVERWRITE_DATE=${BUILD_OVERWRITE_DATE:-false}
fi

mkdir -p repositories

function log {
>&2 printf "\\e[33m%s\\e[0m\\n" "$1"
}

function indent { sed 's/^/    /' 1>&2; }

function checkout {
  if [ -d "repositories/$1" ] ; then
    cd "repositories/$1"
    git reset --hard
    git fetch
  else
    cd "$DIR"
    git clone --single-branch "$2" "repositories/$1"
    cd "repositories/$1"
  fi
  git fetch --tags -f
  cd "$DIR"
}

function checkout_all {
  checkout "gitlab" "https://gitlab.com/gitlab-org/gitlab.git"
  checkout "gitlab-foss" "https://gitlab.com/gitlab-org/gitlab-foss.git"
  checkout "gitlab-pages" "https://gitlab.com/gitlab-org/gitlab-pages.git"
  checkout "gitlab-workhorse" "https://gitlab.com/gitlab-org/gitlab-workhorse.git"
  checkout "gitlab-shell" "https://gitlab.com/gitlab-org/gitlab-shell.git"
  checkout "gitaly" "https://gitlab.com/gitlab-org/gitaly.git"
  checkout "gitlab-svgs" "https://gitlab.com/gitlab-org/gitlab-svgs.git"
  checkout "gitlab-ui" "https://gitlab.com/gitlab-org/gitlab-ui.git"
  checkout "gitlab-csslab" "https://gitlab.com/gitlab-org/csslab.git"
  checkout "www-gitlab-com" "https://gitlab.com/gitlab-com/www-gitlab-com.git"
}

function set_vars {
  cd "$GITLAB_DIR"
  HASH=$(git rev-parse HEAD)
  TMP_DIR="$DIR/tmp"
  TIME=$(git show -s --format=%ct)
  RESULT_DIR="$DIR/results/"
  cd "$DIR"
  rm -rf "$TMP_DIR"
  mkdir -p "$TMP_DIR"
  mkdir -p "$RESULT_DIR"
}

CLOC_IGNORE_LANG="Bourne Shell,Dockerfile,Bourne Again Shell,make,YAML,JSON,Protocol Buffers,Markdown,IPython Notebook,diff,XML,Maven,Python,INI"

function run_cloc {
  cloc . \
    --not-match-f=".+test.go" \
    --exclude-lang="$CLOC_IGNORE_LANG" \
    --exclude-list-file="$DIR/cloc_ignore.txt" \
    --read-lang-def="$DIR/cloc_definitions.txt" > "$1.txt"
}

function run_spec_cloc {
  cloc . \
    --fullpath\
    --match-f "/(spec|features|qa)/" \
    --exclude-lang="$CLOC_IGNORE_LANG" \
    --force-lang-def="$DIR/cloc_spec_definitions.txt" > "$1_spec.txt"
  cloc . \
    --match-f ".+test.go" \
    --exclude-lang="$CLOC_IGNORE_LANG" \
    --force-lang-def="$DIR/cloc_spec_definitions.txt" > "$1_go_spec.txt"
}

function sub_analysis {
  if [ -f "$1" ]; then
    VERSION=$(cat "$1")
    echo "$2: $VERSION"
    cd "$DIR/repositories/$2"
    git checkout "v$VERSION" || git checkout "$VERSION"
    run_cloc "$3/$2"
    run_spec_cloc "$3/$2"
    cd "$CURR_DIR"
  fi
}

function sub_analysis_yarn {
  if [ -f "yarn.lock" ]; then
    if grep -q "$1" yarn.lock; then
      VERSION=$(grep "$1" -A 1 yarn.lock | grep version | grep -E "\\d+.\\d+.\\d+" -o | head -1)
      echo "$2: $VERSION"
      cd "$DIR/repositories/$2"
      git checkout "v$VERSION"
      run_cloc "$3/$2"
      cd "$CURR_DIR"
    fi
  fi
}

function analysis_ce {
  local tag="$1"
  local date="$2"
  git checkout -b "gitlab-history/temp" || git checkout "gitlab-history/temp"
  git branch -D gitlab-history/analysis || echo "Analysis branch doesn't exist"
  git branch "gitlab-history/analysis" "$tag-ee" || git branch "gitlab-history/analysis" "$tag"
  git checkout "gitlab-history/analysis"
  git branch -D "gitlab-history/temp" || echo "Analysis branch doesn't exist"
  mkdir -p "$TMP_DIR/$tag"
  cd "$CURR_DIR"
  HASH=$(git rev-parse HEAD)
  run_cloc "$TMP_DIR/$tag/gitlab"
  run_spec_cloc "$TMP_DIR/$tag/gitlab"
  sub_analysis "GITALY_SERVER_VERSION" "gitaly" "$TMP_DIR/$tag"
  sub_analysis "GITLAB_PAGES_VERSION" "gitlab-pages" "$TMP_DIR/$tag"
  sub_analysis "GITLAB_SHELL_VERSION" "gitlab-shell" "$TMP_DIR/$tag"
  sub_analysis "GITLAB_WORKHORSE_VERSION" "gitlab-workhorse" "$TMP_DIR/$tag"
  sub_analysis_yarn "@gitlab-org/gitlab-svgs" "gitlab-svgs" "$TMP_DIR/$tag"
  sub_analysis_yarn "@gitlab-org/gitlab-ui" "gitlab-ui" "$TMP_DIR/$tag"
  sub_analysis_yarn "@gitlab/csslab" "gitlab-csslab" "$TMP_DIR/$tag"
  sub_analysis_yarn "@gitlab/svgs" "gitlab-svgs" "$TMP_DIR/$tag"
  sub_analysis_yarn "@gitlab/ui" "gitlab-ui" "$TMP_DIR/$tag"
  cd "$TMP_DIR/$tag/"
  cloc --read-lang-def="$DIR/cloc_spec_definitions_combined.txt" --sum-reports --json --out="$tag" ./*.txt
  jq '.' "$tag.lang" | \
    jq --arg commit "$HASH" '.header.commit = $commit' | \
    jq --argjson date "$date" '.header.date = $date' | \
    jq --arg tag "$tag" '.header.tag = $tag' \
    > "$TMP_DIR/all/lang/$tag.json"
}

function analysis_handbook {
  local commit
  local tag="$1"
  local date="$2"
  log "Analysing handbook on $tag, $date"
  cd "$DIR/repositories/www-gitlab-com"
  if [[ "$date" -lt "1431523687" ]]; then
    echo "The Handbook is younger than this!"
  else
    commit=$(git rev-list -n 1 --first-parent --before="$date" origin/master)
    git checkout "$commit"
    cd source/handbook
    node "$DIR/countWords.js" "$DIR/repositories/www-gitlab-com/source/handbook" \
      --output "$TMP_DIR/all/handbook/$tag.json" \
      --tag "$tag" --date "$date"
  fi
}
function analysis {
  cd "$CURR_DIR"
  mkdir -p "$TMP_DIR/all"
  mkdir -p "$TMP_DIR/all/lang"
  if [ "${BUILD_ALL_TAGS}" == "true" ]; then
    cd "$GITLAB_FOSS_DIR"
    touch "$TMP_DIR/tags.txt"
    git tag --sort=v:refname --list | grep -E "^.+\\.0(-ee)?$" | cut -f1 -d- | uniq | sed '/v6.4.0/,$d' >> "$TMP_DIR/tags.txt"
    echo "$SWITCH" >> "$TMP_DIR/tags.txt"
    cd "$GITLAB_DIR"
    git tag --sort=v:refname --list | grep -E "^.+\\.0(-ee)?$" | cut -f1 -d- | uniq | sed -n '/^v6.4.0/,$p' >> "$TMP_DIR/tags.txt"
    cd "$CURR_DIR"
  else
    log "Not building all tags, only a few examples"
    cp "$DIR/tags.example.txt" "$TMP_DIR/tags.txt"
  fi
  echo "master" >> "$TMP_DIR/tags.txt"
  while read -r tag; do
    if [[ "$tag" == "$SWITCH" ]]; then
      CURR_DIR="$GITLAB_DIR"
      echo "SWITCHING OVER TO $CURR_DIR"
      cd "$CURR_DIR"
      continue
    fi
    log "Analysing $tag"
    TIME=$(git log -1 --format=%ct "$tag-ee" || git log -1 --format=%ct "$tag")
    analysis_ce "$tag" "$TIME"
    analysis_handbook "$tag" "$TIME"
    cd "$CURR_DIR"
  done <  "$TMP_DIR/tags.txt"
  cd "$TMP_DIR/all/lang/"
  jq --slurp 'sort_by(.header.date)' ./*.json > "$RESULT_DIR/lang.json"
  cd "$TMP_DIR/all/handbook/"
  jq --slurp 'sort_by(.header.date)' ./*.json > "$RESULT_DIR/handbook.json"
}

log "Check if everything is alright"
yarn install 2>&1 | indent
yarn run lint 2>&1 | indent

HASH=
TMP_DIR=
RESULT_DIR=
TIME=

if [ "${BUILD_OVERWRITE_DATE}" == "true" ]; then
  log "Overwriting build date"
  echo "{}" \
    | jq --argjson buildDate "$CI_BUILD_DATE" '.buildDate = $buildDate' \
    | jq --arg repoUrl "$CI_PROJECT_URL" '.repoUrl = $repoUrl' \
    > assets/data.json
else
  log "Not in CI environment, not overwriting build date"
fi

checkout_all
set_vars
log "Running analysis"
analysis

cd "$DIR"
node --experimental-modules "./formatResults.mjs"

log "Copying static page and zipping"
yarn run generate 2>&1 | indent
find dist -type f -print0 | xargs -0 gzip -f -k
log "Finished build"
tree dist/ | indent
