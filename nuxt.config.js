module.exports = {
  /*
   ** Headers of the page
   */
  head: {
    title: 'GitLab code history',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'GitLab History' },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        href:
          'https://fonts.googleapis.com/css?family=Roboto:300,300italic,700,700italic',
      },
    ],
  },
  /*
   ** Customize the progress bar color
   */
  loading: { color: '#9b4dca' },
  css: ['normalize.css', 'milligram'],
  router: {
    base: process.env.CI ? '/gitlab-history/' : '/',
  },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** Run ESLint on save
     */
    extend(config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
        });
      }
      return config;
    },
  },
};
